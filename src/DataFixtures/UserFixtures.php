<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Entity\User;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        $user = new User();
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                'password'
            ))
            ->setEmail('sponge.bob@sea.com')
            ->setFirstName('Sponge')
            ->setLastName('Bob')
            ->setRoles([User::ROLE_API]);

        $manager->persist($user);

        $manager->flush();
    }
}
