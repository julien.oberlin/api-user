<?php

namespace App\EventListener;

use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\HttpUtils;
use Doctrine\Common\Annotations\AnnotationReader;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

use App\Controller\Api\User\ApiUserController;

class ApiUserRequestListener
{
    /**
     * @var
     */
    private $logger;

    /**
     * @var AnnotationReader
     */
    private $annotationReader;

    public function __construct(LoggerInterface $loggerPost, LoggerInterface $loggerPut, LoggerInterface $loggerGet)
    {
        $this->logger = [
            Request::METHOD_POST => $loggerPost,
            Request::METHOD_PUT => $loggerPut,
        ];
        $this->annotationReader = new AnnotationReader();
    }

    /**
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $currentRoute = $event->getRequest()->get('_route');
        $requestContent = $event->getRequest()->getContent();
        $requestMethod = $event->getRequest()->getMethod();

        $methodsByRouteNameToLog = $this->getRoutesAndMethodsToLog();
        if(\array_key_exists($currentRoute, $methodsByRouteNameToLog)){
            if(\in_array($requestMethod, $methodsByRouteNameToLog[$currentRoute])) {
                if('json' == $event->getRequest()->getFormat($event->getRequest()->headers->get('content-type'))) {
                    if(\array_key_exists($requestMethod, $this->logger)) {
                        $this->logger[$requestMethod]->info(\json_encode(\json_decode($requestContent, true)), ['ip' => $event->getRequest()->getClientIp()]);
                    }
                }
            }
        }
    }

    /**
     * @return array
     */
    private function getRoutesAndMethodsToLog()
    {
        $methodsByRouteNameToLog = [];
        $class = new \ReflectionClass(ApiUserController::class);

        foreach($class->getMethods() as $method) {
            if(ApiUserController::class == $method->class) {
                list($symfonyRoute) = $this->annotationReader->getMethodAnnotations($method);
                $methodsByRouteNameToLog[$symfonyRoute->getName()] = $symfonyRoute->getMethods();
            }
        }
        return $methodsByRouteNameToLog;
    }
}