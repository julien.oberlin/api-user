<?php

namespace App\Controller\Api\User;

use Doctrine\ORM\Query;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

use App\Entity\User;
use App\Form\User\UserUpdateType;
use App\Form\User\UserCreateType;
use App\Controller\Api\ApiController;

/**
 * @Route("/api")
 */
class ApiUserController extends AbstractController
{
    /**
     * @param Request $request
     * @param string $uuid
     *
     * @Route("/user/{uuid}",
     *     name="api_user_get_one",
     *     methods={"GET"},
     *     requirements={"page"="/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"}
     * )
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getOne(Request $request, string $uuid)
    {
        try {
            $queryBuilder = $this->getDoctrine()
                ->getRepository(User::class)
                ->createQueryBuilder('u');

            $queryBuilder
                ->andWhere($queryBuilder->expr()->eq('u.uuid', ':uuid'))
                ->setParameter('uuid', $uuid)
                ->getQuery();

            $result = $queryBuilder->getQuery()->getOneOrNullResult(Query::HYDRATE_ARRAY);
            if($result) {
                return $this->json($result, Response::HTTP_OK);
            }
            return new Response('', Response::HTTP_NOT_FOUND);
        } catch(\Exception $exception) {
            return new Response('', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @param int $offset
     * @param int $limit
     *
     * @Route("/user/{offset}/{limit}",
     *     name="api_user_get_several",
     *     methods={"GET"},
     *     requirements={
     *          "offset"="\d+",
     *          "limit"="\d+"
     *     }
     * )
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function getSeveral(Request $request, int $offset, int $limit)
    {
        try {
            $queryBuilder = $this->getDoctrine()
                ->getRepository(User::class)
                ->createQueryBuilder('u');

            $queryBuilder
                ->setMaxResults($limit)
                ->setFirstResult($offset);

            $result = $queryBuilder->getQuery()->getResult(Query::HYDRATE_ARRAY);
            if($result) {
                return $this->json($result, Response::HTTP_OK);
            }
            return new Response('', Response::HTTP_NOT_FOUND);
        } catch(\Exception $exception) {
            return new Response('', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @Route("/user/{uuid}",
     *     name="api_user_update",
     *     methods={"POST"},
     *     requirements={"page"="/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"}
     * )
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function update(Request $request, string $uuid)
    {
        try {
            $data = \json_decode($request->getContent(), true);

            $queryBuilder = $this->getDoctrine()
                ->getRepository(User::class)
                ->createQueryBuilder('u');
            $queryBuilder->andWhere($queryBuilder->expr()->eq('u.uuid', ':uuid'))
                ->setParameter('uuid', $uuid)
                ->getQuery();

            $query = $queryBuilder->getQuery();
            $user = $query->getOneOrNullResult();

            if($user instanceof User) {
                $form = $this->createForm(UserUpdateType::class, $user);
                $form->submit($data);

                if ($form->isSubmitted() && $form->isValid()) {
                    $em = $this->getDoctrine()->getManager();
                    $em->persist($user);
                    $em->flush();
                    return $this->json($user->toArray(), Response::HTTP_OK);
                }
                return new Response('', Response::HTTP_BAD_REQUEST);
            }
            return new Response('', Response::HTTP_NO_CONTENT);
        } catch(\Exception $exception) {
            return new Response('', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     *
     * @Route("/user",
     *     name="api_user_create",
     *     methods={"PUT"}
     * )
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     */
    public function create(Request $request)
    {
        try {
            $data = \json_decode($request->getContent(), true);
            $user = new User();
            $form = $this->createForm(UserCreateType::class, $user);
            $form->submit($data);

            if ($form->isSubmitted() && $form->isValid()) {
                $queryBuilder = $this->getDoctrine()
                    ->getRepository(User::class)
                    ->createQueryBuilder('u');
                $queryBuilder->andWhere($queryBuilder->expr()->eq('u.email', ':email'))
                    ->setParameter('email', $user->getEmail())
                    ->getQuery();

                $query = $queryBuilder->getQuery();
                $result = $query->getOneOrNullResult();
                if($result instanceof User) {
                    return new Response('', Response::HTTP_NO_CONTENT);
                }

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();
                return $this->json($user->toArray(), Response::HTTP_OK);
            }
            return new Response('', Response::HTTP_BAD_REQUEST);
        } catch(\Exception $exception) {
           return new Response('', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param Request $request
     * @param string $uuid
     *
     * @Route("/user/{uuid}",
     *     name="api_user_delete",
     *     methods={"DELETE"},
     *     requirements={"page"="/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i"}
     * )
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function delete(Request $request, string $uuid)
    {
        try {
            $queryBuilder = $this->getDoctrine()
                ->getRepository(User::class)
                ->createQueryBuilder('u');
            $queryBuilder->andWhere($queryBuilder->expr()->eq('u.uuid', ':uuid'))
                ->setParameter('uuid', $uuid)
                ->getQuery();

            $query = $queryBuilder->getQuery();
            $user = $query->getOneOrNullResult();
            if($user instanceof User) {
                $em = $this->getDoctrine()->getManager();
                $em->remove($user);
                $em->flush();
                return new Response('', Response::HTTP_NO_CONTENT);
            }
            return new Response('', Response::HTTP_BAD_REQUEST);
        } catch(\Exception $exception) {
            return new Response('', Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}