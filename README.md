Install SF5 Api User 
======================================
#### Install docker
1. Install docker
https://docs.docker.com/engine/install/

2. Check docker is installed properly
```shell 
user@linux:~$ sudo docker -v
```
```shell 
user@linux:~$ sudo docker run hello-world
```

#### Install docker-compose
2. Install docker-compose
https://docs.docker.com/compose/install/

1. Check docker-compose is installed properly
```shell 
user@linux:~$ sudo docker-compose -v
```

#### Start containers
1. Browse to docker app directory
```shell
user@linux:~$ cd sf5-api/docker
```
 
1. Stop and purge all other container first to avoid issue
```shell 
user@linux:~/sf5-api/docker$ sudo docker system prune -a && sudo docker images purge && sudo docker stop $(sudo docker ps -a -q) && sudo docker rm $(sudo docker ps -a -q)
```
2. Build and start container
```shell 
user@linux:~/sf5-api/docker$ sudo docker-compose build --no-cache && sudo docker-compose up --build -d
```

#### Nginx vhost
1. Edit your host /etc/hosts to add api.lh and mariadb 
```shell 
user@linux:~$ sudo vim /etc/hosts
```
```shell
127.0.0.1       localhost

127.0.0.1 api.lh mariadb
```

#### App setup
1. Composer
```shell 
user@linux:~/sf5-api$ sudo docker run --tty --rm --interactive -u $(id -u ${USER}):$(id -g ${USER}) --volume $(pwd):/app --volume $(pwd):/app composer:1.6.5 install  
```

2. Create database
```shell 
user@linux:~/sf5-api$ sudo docker exec -ti php-fpm php bin/console doctrine:database:create
```
3. Create table
```shell 
user@linux:~/sf5-api$ sudo docker exec -ti php-fpm php bin/console doctrine:schema:create
```

3. Load fixture (add a user to authenticate on API)
```shell 
user@linux:~/sf5-api$ sudo docker exec -ti php-fpm php bin/console doctrine:fixtures:load
```

4. Check if table is filled with user "Sponge Bob"

Brower localhost:8080 and login into adminer with these credentials :
* System => Mysql
* Server => mariadb
* Username => root
* Password => password
* Database => api_rest

#### Get API route available
```shell 
user@linux:~/sf5-api$ sudo docker exec -ti php-fpm php bin/console debug:router
```

#### Test with Postman
1. Get token
![get-token](./documentation/img/postman.get-token.png "bob")

2. Set token
![get-token](./documentation/img/postman.set-token.png "bob")

3. Add a user
![get-token](./documentation/img/postman.add-user.png "bob")
